pyroot\_zen package
===================

Subpackages
-----------

.. toctree::

    pyroot_zen.entrypoints

Submodules
----------

pyroot\_zen\.RooArgList module
------------------------------

.. automodule:: pyroot_zen.RooArgList
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.RooArgSet module
-----------------------------

.. automodule:: pyroot_zen.RooArgSet
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.RooCategory module
-------------------------------

.. automodule:: pyroot_zen.RooCategory
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.RooWorkspace module
--------------------------------

.. automodule:: pyroot_zen.RooWorkspace
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.TH1 module
-----------------------

.. automodule:: pyroot_zen.TH1
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.TH2 module
-----------------------

.. automodule:: pyroot_zen.TH2
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.TMultiGraph module
-------------------------------

.. automodule:: pyroot_zen.TMultiGraph
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.TObject module
---------------------------

.. automodule:: pyroot_zen.TObject
    :members:
    :undoc-members:
    :show-inheritance:

pyroot\_zen\.utils module
-------------------------

.. automodule:: pyroot_zen.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pyroot_zen
    :members:
    :undoc-members:
    :show-inheritance:
